#
# Copyright (C) 2018-2019 The Google Pixel3ROM Project
# Copyright (C) 2020 Raphielscape LLC. and Haruka LLC.
#
# Licensed under the Apache License, Version 2.0 (the License);
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an AS IS BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
#

# Default Permissions
PRODUCT_PACKAGES += \
    default_permissions_allowlist

# App Permissions
PRODUCT_PACKAGES += \
    com.android.imsserviceentitlement \
    com.android.omadm.service \
    com.android.sdm.plugins.connmo \
    com.android.sdm.plugins.dcmo \
    com.android.sdm.plugins.diagmon \
    com.android.sdm.plugins.sprintdm \
    com.android.sdm.plugins.usccdm \
    com.customermobile.preload.vzw \
    com.google.android.odad \
    com.google.omadm.trigger \
    com.verizon.apn \
    com.verizon.services \
    com.verizon.services \
    privapp-permissions-google-p \
    split-permissions-google \
    vzw_mvs_permissions

# Preffered app list
PRODUCT_PACKAGES += \
    preferred_apps_google

# Sysconfig
PRODUCT_PACKAGES += \
    allowlist_com.android.omadm.service \
    google_build \
    google-hiddenapi-package-whitelist \
    google-staged-installer-whitelist \
    google \
    nexus \
    nga \
    pixel_2016_exclusive \
    pixel_experience_2017 \
    pixel_experience_2018 \
    pixel_experience_2019_midyear \
    pixel_experience_2019 \
    pixel_experience_2020_midyear \
    pixel_experience_2020 \
    preinstalled-packages-product-pixel-2017-and-newer

# GDialer Support
PRODUCT_PACKAGES += \
    com.google.android.dialer.support
